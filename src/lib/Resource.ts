import {ResourcyError} from './Exception/ResourcyError';
import {ResourcyTypeError} from './Exception/ResourcyTypeError';
import {DataContainer, TypeMeta} from './types';


export const DataSymbol = Symbol('types');

type StaticConstructor<T> = new () => T;

export abstract class Resource
{
	[name: string]: any;
	[DataSymbol]: DataContainer<any, any>;
	
	
	public static factory<T extends Resource>(this: StaticConstructor<T>, data: {[name: string]: any}): T
	{
		// Make sure we don't change the raw data
		data = Object.assign({}, data);
		
		const target = new this();
		const types = target[DataSymbol];
		
		for (let key in types) {
			if (types.hasOwnProperty(key)) {
				const type: TypeMeta<any, any> = types[key];
				
				if (!data.hasOwnProperty(key)) {
					if (type.optional) {
						continue;
					}
					
					throw new ResourcyError(`Key '${key}' is required`);
				}
				
				if (type.before && typeof type.before === 'function') {
					data[key] = type.before(data[key]);
				}
				
				try {
					target[key] = Resource.map(type.type, data[key]);
				} catch (error) {
					throw new ResourcyTypeError(key, type.type, error);
				}
				
				if (type.after && typeof type.after === 'function') {
					target[key] = type.after(target[key]);
				}
			}
		}
		
		return target;
	}
	
	
	private static map<T>(type: T|T[]|any|any[], data: any|any[]): T|T[]
	{
		if (Array.isArray(type) && !Array.isArray(data)) {
			throw new Error('Type does not match data');
		} else if (!Array.isArray(type) && Array.isArray(data)) {
			throw new Error('Type does not match data');
		}
		
		if (Array.isArray(type) && Array.isArray(data)) {
			return data.map<T>((item) => Resource.map(type[0], item) as T);
		} else if (type === Object) {
			return Object.assign({}, data);
		} else if (type === Number || type === Boolean || type === String) {
			return type(data);
		} else if (type === Date) {
			if (typeof data === 'string') {
				data = Date.parse(data);
				
				if (isNaN(data)) {
					throw new Error('Date is not parseable');
				}
			}
			
			return new type(data);
		} else if (new type instanceof Resource) {
			return type.factory(data);
		} else {
			return new type(data);
		}
	};
}
