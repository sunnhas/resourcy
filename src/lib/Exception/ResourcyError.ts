export class ResourcyError extends Error
{
	constructor(message: string)
	{
		super(message);
		
		Object.setPrototypeOf(this, ResourcyError.prototype);
		
		this.name = 'ResourcyError';
	}
}
