import {parseTypeName} from '../../util/typeName';


export class ResourcyTypeError extends Error
{
	private error: Error;
	
	
	constructor(key: string, type: any|any[], error: Error)
	{
		super();
		
		Object.setPrototypeOf(this, ResourcyTypeError.prototype);
		
		this.name = 'ResourcyTypeError';
		this.message = `Data at '${key}' does not match type '${this.typeToString(type)}'`;
		this.error = error;
	}
	
	
	private typeToString(type: any|any[]): string
	{
		if (Array.isArray(type)) {
			return `${parseTypeName(type[0])}[]`;
		}
		
		return parseTypeName(type);
	}
}
