import {DataSymbol} from './Resource';
import {DataContainer, IPropertyOptions, Type, TypeMeta} from './types';


export const Property = <U, T extends Type|Type[]>(type: T, optional: boolean = false, options?: IPropertyOptions<U>): PropertyDecorator => {
	options = {
		optional: optional,
		...options,
	};
	
	const typeData: TypeMeta<T, U> = {
		type,
		optional: !! options.optional,
		...options,
	};
	
	return (target: Object, key: string|symbol) => {
		const types: DataContainer<any, any> = {
			...Reflect.get(target, DataSymbol) || {},
			[key]: typeData,
		};
		
		Reflect.defineProperty(target, DataSymbol, {
			value:        types,
			enumerable:   true,
			configurable: true,
		});
	};
};
