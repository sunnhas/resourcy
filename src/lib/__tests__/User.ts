import {Resource} from '../Resource';
import {Property} from '../Property';


export class SomeType
{
	private data: number;
	
	constructor(data: number)
	{
		this.data = data;
	}
}


export class User extends Resource
{
	@Property(Number)
	public id: number;
	
	@Property(String)
	public name: string;
	
	@Property(Boolean)
	public isAdmin: boolean;
	
	@Property(Object)
	public friend: object;
	
	@Property(Date)
	public created_at: Date;
	
	@Property(Date)
	public updated_at: Date;
	
	@Property(SomeType)
	public someType: SomeType;
	
	@Property(User, true)
	public user?: User;
	
	@Property([User], true)
	public users?: User[];
	
	@Property(Number, true, {before: (data) => data.replace('hello', '')})
	public iWillFail?: number;
}
