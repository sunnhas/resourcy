import {DataSymbol} from '../Resource';
import {DataContainer} from '../types';
import {User} from './User';


describe('property decorator', () => {
	const types: DataContainer<any> = User.prototype[DataSymbol];
	
	it('should have correct property types', () => {
		expect(types.id.type).toBe(Number);
		expect(types.name.type).toBe(String);
		expect(types.isAdmin.type).toBe(Boolean);
		expect(types.friend.type).toBe(Object);
		expect(types.user.type).toBe(User);
	});
	
	it('should have set optional', () => {
		expect(types.id.optional).toBeFalsy();
		expect(types.user.optional).toBeTruthy();
	});
});
