import {Property} from '../Property';
import {Resource} from '../Resource';
import {ResourcyError} from '../Exception/ResourcyError';
import {SomeType, User} from './User';


const userData = {
	id:         10,
	name:       'Full name',
	isAdmin:    false,
	created_at: 1530345678,
	updated_at: '1970-01-19T17:05:45.678Z',
	someType:   250,
	
	friend: {
		a: 'a',
		b: 'b',
	},
};

describe('Resource', () => {
	it('should map native types', () => {
		const user = User.factory(userData);
		
		expect(user).toBeInstanceOf(User);
		expect(user).toHaveProperty('id', userData.id);
		expect(user).toHaveProperty('name', userData.name);
		expect(user).toHaveProperty('isAdmin', userData.isAdmin);
		expect(user).toHaveProperty('friend', userData.friend);
		expect(user).toHaveProperty('created_at');
		expect(user).toHaveProperty('updated_at');
		expect(user).toHaveProperty('someType');
		
		expect(user.created_at).toBeInstanceOf(Date);
		expect(user.created_at.getTime()).toBe(userData.created_at);
		expect(user.updated_at).toBeInstanceOf(Date);
		expect(user.updated_at.getTime()).toBe(Date.parse(userData.updated_at));
		expect(user.someType).toBeInstanceOf(SomeType);
		
		expect(user).not.toHaveProperty('user');
		expect(user).not.toHaveProperty('users');
	});
	
	it('should map nested types', () => {
		const user = User.factory({
			...userData,
			user: userData,
		});
		
		expect(user).toHaveProperty('user');
		expect(user.user).toBeInstanceOf(User);
	});
	
	it('should map arrays of nested types', () => {
		const user = User.factory({
			...userData,
			users: [
				userData,
				userData,
			],
		});
		
		expect(user).toHaveProperty('users');
		expect(user.users).toHaveLength(2);
		
		user.users ? user.users.forEach((user) => expect(user).toBeInstanceOf(User)) : null;
	});
	
	it('should handle before/after hooks', () => {
		class Test extends Resource
		{
			@Property(Date, false, {before: (data) => data * 1000})
			public date: Date;
			
			@Property<Date, DateConstructor>(Date, false, {
				after: (data) => new Date(data.getTime() / 1000),
			})
			public date2: Date;
		}
		
		const testData = {
			date: 1530345,
			date2: 1530345000,
		};
		
		const t = Test.factory(testData);
		
		expect(t).toHaveProperty('date', new Date(testData.date * 1000));
		expect(t).toHaveProperty('date2', new Date(testData.date2 / 1000));
	});
	
	it('should fail on missing data', () => {
		expect(() => User.factory({})).toThrowError(ResourcyError);
	});
	
	it('should throw error on wrong type', () => {
		expect(() => {
			User.factory({
				...userData,
				users: userData
			});
		}).toThrowErrorMatchingSnapshot();
		
		expect(() => {
			User.factory({
				...userData,
				user: [userData]
			});
		}).toThrowErrorMatchingSnapshot();
		
		expect(() => {
			User.factory({
				...userData,
				updated_at: '1970-gj1-18Z',
			});
		}).toThrowErrorMatchingSnapshot();
	});
});
