export type Type = {new(data?: any): any};
export type DataContainer<T extends Type|Type[], U> = {[name: string]: TypeMeta<T, U>};

export interface IPropertyOptions<T>
{
	optional?: boolean;
	before?: (data: any) => any;
	after?: (data: T) => T;
}

export interface TypeMeta<T extends Type|Type[], U> extends IPropertyOptions<U>
{
	type: T;
	optional: boolean;
}
