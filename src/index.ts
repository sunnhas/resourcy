import {Resource} from './lib/Resource';


export {Resource} from './lib/Resource';
export {Property} from './lib/Property';

export const Entity = Resource;
export type Entity = Resource;
