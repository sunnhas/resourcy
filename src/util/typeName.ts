export const parseTypeName = <T>(type: T): string =>
{
    const match = (/^(?:class|function) (\w+)[( ]/).exec(type.toString());
    if (match) {
        return match[1];
    }

    return 'unknown';
};
