import {parseTypeName} from '../typeName';


class TestClass {}

describe('util typeName', () => {
	it('should parse correct type name', function () {
		expect(parseTypeName(Number)).toBe('Number');
		expect(parseTypeName(TestClass)).toBe('TestClass');
	});
	
	it('should parse to unknown', () => {
		expect(parseTypeName('test')).toBe('unknown');
	});
});
