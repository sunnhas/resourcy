# Resourcy
> A zero dependency JSON/object mapper to resource objects

[![pipeline status][gitlab-pipeline]][gitlab-commits]
[![coverage report][gitlab-coverage]][gitlab-commits]
[![NPM Version][npm-image]][npm-url]
[![Node Version][node-version]][npm-url]
[![Downloads Stats][npm-downloads]][npm-url]

Resourcy is a zero dependency object mapper.
It enables easy mapping to actual resource classes by using decorators to describe the resource.

This enables easy mapping from API JSON responses to your own resources.


## Installation
NPM:

```sh
npm install resourcy --save
```

Yarn:

```sh
yarn add resourcy
```


## Usage example
Ever had to call an API and use the response resource?
Tired of having unstructured helper methods to parse the data laying around?
Resourcy can help structure your resources by mapping the data from the response into an actual resource class.

Assume you would retrieve some user data by calling `//example.com/user/[id]`.

```json
// response from //example.com/user/[id]
{
	"id": 10,
	"name": "John Doe",
	"created_at": 1530345678
}
```

Now define a user resource

```typescript
// User.ts
import {Resource, Property} from 'resourcy';

export class User extends Resource
{
	@Property(Number)
	public id: number;
	
	@Property(String)
	public name: string;
	
	@Property(Date)
	public created_at: Date;
	
	// Create resource specific helper methods if needed
}
```

Now there is only the simple mapping left.

```typescript
// index.ts
import {User} from './User.ts'

api.get('/user/[id]')
	.then((response) => {
		const user = User.factory(response.json());
		
		expect(user).toBeInstanceOf(User); // true
		expect(user.id).toBeInstanceOf(Number); // true
		expect(user.name).toBeInstanceOf(String); // true
		expect(user.created_at).toBeInstanceOf(Date); // true
	});
```


## Resourcy API
Use by:

```typescript
import {Resource, Property} from 'resourcy';
```

### resourcy.Resource (abstract class)
Abstract class to define a Resource class.
Has one static method `Resource.factory` to map the Resource against a given dataset.

#### resourcy.Resource.factory<T extends Resource>(data: {[name: string]: any}): T
Returns a mapped Resource class.
The argument is an object of the raw data which should be mapped.

### resourcy.Entity (Deprecated)
Alias for [resourcy.Resource](#resourcy.resource-(abstract-class)).
Deprecated - will be removed in a later version.

### resourcy.Property(type, [optional = false], [options])
Decorator used to specify information on a given Resource property.
Define which type the property is some other optional options.

#### resourcy.Property.optional
As default the properties is required.
This means they will throw an `ResourcyError` if they are not given.

#### resourcy.Property.options
Additional options can be given as an object.
The options are shown below with their default values.

```typescript
interface IPropertyOptions<T>
{
	// Defines if the property on the resource should be optional.
	// If set to false, they will throw an ResourcyError if they are not given.
	// Default: false
	optional?: boolean;
	
	// Function to handle/manipulate the data before mapping.
	// The funtion retrieves one argument of the raw type and should return raw data.
	before?: (data: any) => any;
	
	// Function to handle/manipulate the data after is has been mapped.
	// The function retrieves one argument of the type specified and should return something of the same type.
	after?: (data: T) => T;
}
```


## Development setup
After cloning/forking run the following commands to setup the project and run tests.

```sh
yarn install
yarn test
```


## Maintainer
[@sunnhas](https://gitlab.com/sunnhas)

Distributed under the MIT license.
See [LICENSE](./LICENSE) for more information.


## Contributing
1. Fork it
2. Create your feature branch (`git checkout -b feature/fooBar`)
3. Commit your changes (`git commit -am 'Add some fooBar'`)
4. Push to the branch (`git push origin feature/fooBar`)
5. Create a new Pull Request

<!-- Markdown link & img dfn's -->
[gitlab-commits]: https://gitlab.com/sunnhas/resourcy/commits/master
[gitlab-pipeline]: https://gitlab.com/sunnhas/resourcy/badges/master/pipeline.svg
[gitlab-coverage]: https://gitlab.com/sunnhas/resourcy/badges/master/coverage.svg
[node-version]: https://img.shields.io/node/v/resourcy.svg?style=flat-square
[npm-image]: https://img.shields.io/npm/v/resourcy.svg?style=flat-square
[npm-url]: https://npmjs.org/package/resourcy
[npm-downloads]: https://img.shields.io/npm/dw/resourcy.svg?style=flat-square
